let grade = [98.5, 94.3, 89.2, 90.0]
console.log(grade)

/* 
    Object is a collection of related data and functionality, 
    usually representing real-world objects

    Curly braces - initializer for creating objects
    {
        key1: value1,
        key2: value2,
        ...
    }

    Key - identifier 
    value - data associated with a particular key

    Dot notation - used to access the property and value of an object; preferred notation
*/

grade = {
    math: 98.5, 
    english: 94.3,
    science: 90.2,
    mapeh: 89.2
}

console.log(grade)
console.log(grade.math)
console.log(grade.english, grade.mapeh)

const cellphone = {
    brand: "Huawei",
    color: "Blue",
    mfd: "2015"
}

console.log(cellphone)
console.log(typeof cellphone)

const student = {
    firstName: "John",
    lastName: "Smith",
    mobileNumber: 09123456789,
    location: {
        city: "Tokyo",
        country: "Japan"
    },
    email: ["john@mail.com", "smith@mail.com"],
    fullname: function () { 
        return `${this.firstName} ${this.lastName}`
    }
}

console.log(student.fullname())
console.log(student.location.city)
console.log(student.email[0])

const contactList = [
    {
        firstName: "John",
        lastName: "Smith",
        location: "Japan"    
    },
    {
        firstName: "Jane",
        lastName: "Smith",
        location: "Japan" 
    },
    {
        firstName: "Jasmin",
        lastName: "Smith",
        location: "Japan" 
    }
]

console.log(contactList[1]);
console.log(contactList[2].firstName);

/*
    Constructor function - create reusable used to create several objects;
        useful for creating copies/instances of an object

    object literals - curly braces
    instance - concrete occurence of an object
*/

function Laptop(name, mtd) {
    this.name = name
    this.mtd = mtd

    // Performs different commands
    // console.log(this)
}

const laptop1 = new Laptop("Asus", 2021)
console.log(laptop1)

const laptop2 = new Laptop("Toshiba", 1997)
console.log(laptop2)

const laptop = [laptop1, laptop2]
console.log(laptop)

const car = {}
car.name = "Honda"
car["manufacturedDate"] = 2018

console.log(car)
car.name = "Volvo"
console.log(car)

delete car.name
console.log(car)

const person = {
    username: "Johnny",
    talk: function () {
        console.log(`Hello, I am ${this.username}`)
    }
}

person.talk()
person.walk = function () {
    console.log(`${this.username} walked 25 steps forward`)
}

person.walk()

const friend = {
    firstName: "Joe",
    lastName: "Doe",
    address: {
        city: "Austin",
        state: "Texas"
    },
    email: ["joe@mail.com", "joedoe@mail.com"],
    introduce: function () {
        console.log(`Hello! My name is ${this.firstName} ${this.lastName}`)
    }
}

const pokemon = {
    name: "Pikachu",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function (target) {
        console.log(`${this.name} tackled ${target.name}`)
        target.health -= this.attack
        console.log(`${target.name}'s health is now reduced to ${target.health}`)

        if (target.health <= 0)
            console.log(`${target.name} fainted`)
    }
}

function Pokemon(name, level) {
    this.name = name
    this.level = level
    this.health = 2 * level
    this.attack = level
}

const target = new Pokemon("Gengar", 30)
target.wave = function (target) {
    console.log(`${this.name} tackled ${target.name}`)
    target.health -= this.attack
    console.log(`${target.name}'s health is now reduced to ${target.health}`)
    
    if (target.health <= 0)
        console.log(`${target.name} fainted`)
}

pokemon.tackle(target)
target.wave(pokemon)
pokemon.tackle(target)