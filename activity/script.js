const trainer = {
    name: "Johnny",
    age: 22,
    pokemon: ["Pikachu", "Balbasaur", "Gengar"],
    friend: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function() {
        const num = Math.floor(Math.random() * this.pokemon.length)
        console.log(`${this.pokemon[num]}! I choose you!`)
        return num
    }
}

console.log(trainer)
console.log("Result of dot notation:")
console.log(trainer.name)
console.log("Result of square bracket notation:")
console.log(trainer["pokemon"])

function Pokemon(name, level) {
    this.name = name
    this.level = level
    this.health = 3 * level
    this.attack = 2 * level
    this.tackle = function (target) {
        console.log(`${this.name} tackled ${target.name}`)
        target.health -= this.attack
        console.log(`${target.name}'s health is now reduced to ${target.health}`)
        
        if (target.health <= 0)
            this.faint(target)
    },
    this.faint = function (target) {
        console.log(`${target.name} has fainted`)
    }
}

console.log("Result of talk method")
const pokemon = [], index = trainer.talk()
let level = 40
for (let x in trainer.pokemon) {
    pokemon.push(new Pokemon(trainer.pokemon[x], level))
    level += 10
    console.log(pokemon[x])
}

const chosenPokemon = pokemon[index]
const opponent = new Pokemon("Dynamax", 50)

chosenPokemon.tackle(opponent)
console.log(opponent)
opponent.tackle(chosenPokemon)
chosenPokemon.tackle(opponent)
console.log(opponent)